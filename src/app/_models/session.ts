import { User } from './';

export class Session {
  token: string;
  user: User;
}
