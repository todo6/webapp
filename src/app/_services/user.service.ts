import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../_models';
import { environment } from '../../environments/environment';

const { host }: { host: string } = environment;

interface UserResponse {
  user: User;
  message: string;
}
@Injectable({ providedIn: 'root' })
export class UserService {
  constructor(private http: HttpClient) {}

  public me(): Observable<User> {
    return this.http.get<User>(`${host}/users/me`);
  }

  public signUp(user: User): Observable<User> {
    return this.http.post<User>(`${host}/auth/sign-up`, user);
  }
}
