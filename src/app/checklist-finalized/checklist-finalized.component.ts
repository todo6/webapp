import { Component, OnInit } from '@angular/core';
import { ChecklistService } from '../_services';
import { Task } from '../_models';

@Component({
  selector: 'app-checklist-finalized',
  templateUrl: './checklist-finalized.component.html',
  styleUrls: ['./checklist-finalized.component.css']
})
export class ChecklistFinalizedComponent implements OnInit {
  checklist;
  constructor(
    private checklistService: ChecklistService
  ) { }

  ngOnInit(): void {
    this.checklistService.getFinalizedChecklist().subscribe((tasks: Task[]) => {
      return this.checklist = tasks;
    });
  }
  btnDeleteTask_click(check: boolean): void {
    this.checklistService.deleteChecklistByCheck(check);
  }


}
