import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChecklistFinalizedComponent } from './checklist-finalized.component';

describe('ChecklistFinalizedComponent', () => {
  let component: ChecklistFinalizedComponent;
  let fixture: ComponentFixture<ChecklistFinalizedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChecklistFinalizedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChecklistFinalizedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
