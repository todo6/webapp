import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UserService } from '../_services';
import { User } from '../_models';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  formGroup: FormGroup;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      txtName: ['', [Validators.required, Validators.maxLength(60)]],
      txtLastName: ['', [Validators.required, Validators.maxLength(60)]],
      txtEmail: ['', [Validators.required, Validators.maxLength(100)]],
      txtPassword: ['', [Validators.required, Validators.maxLength(40)]],
    });
  }

  onSubmit(): void {
    const { txtName, txtLastName, txtEmail, txtPassword } = this.formGroup.value;
    const user: User = {
      password: txtPassword,
      email: txtEmail,
      name: txtName,
      lastName: txtLastName,
    };
    this.userService
      .signUp(user)
      .pipe(first())
      .subscribe(() => this.router.navigate(['auth/login'], { queryParams: { email: txtEmail } } ));

  }
}
