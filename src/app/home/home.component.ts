import { Component, OnInit } from '@angular/core';
import { ChecklistService } from '../_services';
import { Task } from '../_models';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  checklist: Task[];
  txtTitle;
  txtDescription;
  constructor(
    private checklistService: ChecklistService
  ) { }

  ngOnInit(): void {
    this.checklistService.getPendingChecklist().subscribe((tasks: Task[]) => (this.checklist = tasks));
  }

  btnNewTask_click(): void {
    const newTask: Task = {
      title: this.txtTitle,
      description: this.txtDescription,
    };
    this.checklistService.addTask(newTask).subscribe((item) => {
      this.checklist.push(item.task);
      this.txtTitle = '';
      this.txtDescription = '';
    });
  }

}
