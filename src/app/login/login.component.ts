import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../_services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email;
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,

  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      console.log({params});
      this.email = params.email;
    });
    console.log('test', this.email);
    this.loginForm = this.formBuilder.group({
      txtEmail: ['', Validators.required],
      txtPassword: ['', Validators.required],
    });
    if (this.email) {
      this.f.txtEmail.setValue(this.email);
    }
  }

  get f(): any {
    return this.loginForm.controls;
  }

  public onSubmit(): void {
    if (this.loginForm.invalid) {
      return;
    }
    this.authenticationService
      .login(this.f.txtEmail.value, this.f.txtPassword.value)
      .pipe(first())
      .subscribe((data) => {
        console.log({ user: data.user });
        this.router.navigate(['home']);
      });
  }

}
