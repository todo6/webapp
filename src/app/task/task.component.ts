import { Component, OnInit, Input } from '@angular/core';
import { ChecklistService } from '../_services';
import { Task } from '../_models';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  @Input() task;
  txtTitle;
  txtDescription;
  constructor(
    private checklistService: ChecklistService,
  ) { }

  ngOnInit(): void {
    this.txtTitle = this.task.title;
    this.txtDescription = this.task.description;
  }

  btnDelete_click(): void {
    console.log('Eliminado', this.task._id);
    this.checklistService.deleteChecklist(this.task._id).subscribe(() => {
      delete this.task._id;
    });
  }

  btnEditTask_click(): void {
    const editTask: Task = {
      _id: this.task._id,
      title: this.txtTitle,
      description: this.txtDescription,
    };
    this.checklistService.editTask(editTask).subscribe(() => {
      this.task.title = this.txtTitle;
      this.task.description = this.txtDescription;
    });
  }

  btnEditStatus_click(check: boolean): void {
    const editTask: Task = {
      _id: this.task._id,
      check,
    };
    this.checklistService.editTask(editTask).subscribe(() => {
      delete this.task._id;
    });
  }
}
